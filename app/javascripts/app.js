var accounts;
var account;
var balance;
var network_id;
var promises = [];

function setStatus(message) {
  var status = document.getElementById("status");
  status.innerHTML = message;
};



function getMovie(movie_index) {
  return  SciFi.deployed().getMovieNameAndBid.call(movie_index, {from: account}).then(function(result){
    return { name: result[0], bid: result[1].toNumber() };
  });
}

function refreshMovies() {
  var moviesHtml = document.getElementById('movies');
  moviesHtml.innerHTML = '';
  promises = [];

  SciFi.deployed().getMovieNum.call({from: account}).then(function(movie_num) {
   for(i=0; i < movie_num.valueOf(); i++) {
      promises.push(getMovie(i));
    }
    Promise.all(promises).then(function(movies) {

      var moviesSorted = movies.sort(sortByBid).slice(0, 20);
      for(i=0; i < moviesSorted.length; i++) {
        moviesHtml.innerHTML += '<tr class="movie"><td>' + web3.toAscii(moviesSorted[i].name) + '</td><td>' + web3.fromWei(moviesSorted[i].bid) + ' ETHs</td></tr>';
      }
    });
    return true;
  }).catch(function(e) {
    console.log(e);
    setStatus("Error getting movies; see log.");
  });
  
};

function sortByBid(a, b) {
  return (a.bid > b.bid) ? -1 : (( a.bid < b.bid) ? 1 : 0);
}

function sendMovie() {
  var movieName = document.getElementById("movieName").value;
  var ethAmount = document.getElementById("ethAmount").value;

  setStatus("Processing vote... (please wait)");
  /*console.log(movieName);
  console.log(ethAmount);
  console.log(web3.toHex(movieName));
  console.log(web3.toWei(ethAmount, 'ether'));*/
   SciFi.deployed().vote(web3.toHex(movieName), {from: account, value: web3.toWei(ethAmount, 'ether')}).then(function() {
    setStatus("Movie " + movieName + " voted!");
    //refreshMovies();
  }).catch(function(e) {
    console.log(e);
    setStatus("Error voting movie; see log.");
  });
};

window.onload = function() {
  web3.eth.getAccounts(function(err, accs) {
    if (err != null) {
      alert("There was an error fetching your accounts.");
      return;
    }

    if (accs.length == 0) {
      alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
      return;
    }

    accounts = accs;
    account = accounts[0];

   web3.version.getNetwork(function(error, network_id){
     SciFi.setNetwork(network_id);
    
      switch(network_id) {
          case '1':
              network = 'Main Ethereum'
              break;
          case '2':
              network = 'Morden Test'
              break;
          default:
              network = 'Test RPC';
              break;
      }
      document.getElementById('network').innerHTML = network;

      refreshMovies();
      
      SciFi.deployed().MovieVoted().watch(function(error, result){
        alertify.notify('User ' + result.args._from + ' voted ' + web3.toAscii(result.args.movieName) + ' giving ' + web3.fromWei(result.args.bids, 'ether') + 'ETHs', 'success', 10, function(){ });
        refreshMovies();
        return true;
      });
   });
  });
  
}

