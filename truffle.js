module.exports = {
  build: {
    "index.html": "index.html",
    "app.js": [
      "javascripts/app.js",
      "javascripts/alertify.min.js",
    ],
    "app.css": [
      "stylesheets/app.css",
      "stylesheets/alertify.min.css",
    ],
    "images/": "images/"
  },
  rpc: {
    host: "localhost",
    port: 8545
  },
  networks: {
    "live": {
      network_id: 1,
    },
    "morden": {
      network_id: 2,
    },
    "development": {
      network_id: "default"
    }
  }
};
