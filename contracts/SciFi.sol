contract SciFi {
    mapping(bytes32 => uint) public bids;
    bytes32[1000000] public movies;
    uint public movie_num;

    event MovieVoted(address indexed _from, bytes32 indexed movieName, uint indexed bids);

    function vote(bytes32 movieName) {
        if (msg.value == 0)
            return;

        uint val = bids[movieName];

        if (val == 0) {
            movies[movie_num++] = movieName;
        }

        bids[movieName] += msg.value;

        MovieVoted(msg.sender, movieName, msg.value);
    }

    function getMovieNum() returns(uint) {
        return movie_num;
    }

    function getMovieName(uint i) returns(bytes32) {
        return movies[i];
    }

    function getMovieNameAndBid(uint i) returns(bytes32, uint) {
        return (movies[i], bids[movies[i]]);
    }
}